#!/bin/sh
## run as ./ltmine.sh
## stratum proxy
sudo apt-get install git python-dev screen
if [ ! -d stratum-mining-proxy ]; then
git clone https://github.com/CryptoManiac/stratum-mining-proxy.git
fi
if [ $? -eq 0 ]; then
cd stratum-mining-proxy
sudo python distribute_setup.py
cd litecoin_scrypt
sudo python setup.py install
cd .. 
sudo python setup.py develop 
cd
cat > "liteproxy.sh" <<END
cd ~/stratum-mining-proxy 
screen -dmS proxy ./mining_proxy.py -pa scrypt
cd 
END
cd
chmod a+x liteproxy.sh
./liteproxy.sh
echo 'To update stratum, cd into the folder and "git pull"'
echo 'Stratum Installed, Call liteproxy.sh directly next time.'
fi
## cpu miner
cd
sudo apt-get install automake build-essential libcurl4-openssl-dev
if [ ! -d cpuminer ]; then
git clone git://github.com/pooler/cpuminer.git
fi
cd cpuminer
./autogen.sh
./configure CFLAGS="-O3"
make
if [ $? -eq 0 ]; then
cd
cat > "liteminer.sh" <<END
url='http://localhost:8332/'
user='user'
pass='pass'
threads=2
cd ~/cpuminer
screen -dmS miner nice -n 20 ./minerd -t \$threads -o \$url -O \$user:\$pass
cd
END
fi
cd
if [ -a liteminer.sh ]; then
chmod a+x liteminer.sh
./liteminer.sh
echo 'Installed! Call liteminer.sh directly next time.'
fi

